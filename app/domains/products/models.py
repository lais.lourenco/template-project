from database import db
from sqlalchemy import String, ForeignKey
from sqlalchemy.orm import relationship


class Product(db.Model):
    __tablename__ = 'products'

    id = db.Column(db.String(36), primary_key=True)
    name = db.Column(db.String(80))
    price = db.Column(db.Float(10))
    category_id = db.Column(String, ForeignKey('category_products.id'))
    category = relationship('CategoryProduct')


    def serialize(self):
        return {
            'id': self.id,
            'name': self.name,
        }
