from app.domains.products.models import Product
from database import db
from sqlalchemy.orm import relationship


class CategoryProduct(db.Model):
    __tablename__ = 'category_products'

    id = db.Column(db.String(36), primary_key=True)
    name = db.Column(db.String(80))
    product = relationship(Product, backref="products")

    def serialize(self):
        return {
            'id': self.id,
            'name': self.name,
        }
